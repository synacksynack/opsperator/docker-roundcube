# k8s Roundcube

Roundcube image.

Diverts from https://gitlab.com/synacksynack/opsperator/docker-php

Depends on a Cyrus server, such as
https://gitlab.com/synacksynack/opsperator/docker-cyrus

Depends on an OpenLDAP server, such as
https://gitlab.com/synacksynack/opsperator/docker-openldap

Depends on a Postfix server, such as
https://gitlab.com/synacksynack/opsperator/docker-postfix

Build with:

```
$ make build
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name           |    Description                | Default                                                     | Inherited From    |
| :------------------------- | ----------------------------- | ----------------------------------------------------------- | ----------------- |
|  `APACHE_DOMAIN`           | Roundcube ServerName          | `mail.$MAIL_DOMAIN`                                         | opsperator/apache |
|  `APACHE_HTTP_PORT`        | Roundcube HTTP(s) Port        | `8080`                                                      | opsperator/apache |
|  `DBURL`                   | Roundcube Database URL        | undef, generated from `POSTGRESQL_` or `MYSQL_` env vars    |                   |
|  `DES_KEY`                 | Roundcube encryption key      | `stringshouldbe24charslog`                                  |                   |
|  `IMAP_HOST`               | Roundcube IMAP Host           | `imap.$MAIL_DOMAIN`                                         |                   |
|  `IMAP_PORT`               | Roundcube IMAP Port           | `143`                                                       |                   |
|  `IMAP_PROTO`              | Roundcube IMAP Protocol       | `imap`                                                      |                   |
|  `IMAP_VENDOR`             | Roundcube IMAP Vendor         | `none`                                                      |                   |
|  `MAIL_DOMAIN`             | Roundcube IMAP Domain         | `demo.local`                                                | opsperator/apache |
|  `MYSQL_DATABASE`          | MySQL Database Name           | `roundcube`                                                 |                   |
|  `MYSQL_HOST`              | MySQL Database Host           | `127.0.0.1`                                                 |                   |
|  `MYSQL_PASSWORD`          | MySQL Database Password       | undef                                                       |                   |
|  `MYSQL_PORT`              | MySQL Database Port           | `3306`                                                      |                   |
|  `MYSQL_USER`              | MySQL Database Username       | `roundcube`                                                 |                   |
|  `ONLY_TRUST_KUBE_CA`      | Don't trust base image CAs    | `false`, any other value disables ca-certificates CAs       | opsperator/apache |
|  `PHP_ERRORS_LOG`          | PHP Errors Logs Output        | `/proc/self/fd/2`                                           | opsperator/php    |
|  `PHP_MAX_EXECUTION_TIME`  | PHP Max Execution Time        | `30` seconds                                                | opsperator/php    |
|  `PHP_MAX_FILE_UPLOADS`    | PHP Max File Uploads          | `20`                                                        | opsperator/php    |
|  `PHP_MAX_POST_SIZE`       | PHP Max Post Size             | `8M`                                                        | opsperator/php    |
|  `PHP_MAX_UPLOAD_FILESIZE` | PHP Max Upload File Size      | `2M`                                                        | opsperator/php    |
|  `PHP_MEMORY_LIMIT`        | PHP Memory Limit              | `-1` (no limitation)                                        | opsperator/php    |
|  `PUBLIC_PROTO`            | Apache Public Proto           | `http`                                                      | opsperator/apache |
|  `POSTGRES_DB`             | Postgres Database Name        | `roundcube`                                                 |                   |
|  `POSTGRES_HOST`           | Postgres Database Host        | `127.0.0.1`                                                 |                   |
|  `POSTGRES_PASSWORD`       | Postgres Database Password    | undef                                                       |                   |
|  `POSTGRES_PORT`           | Postgres Database Port        | `5432`                                                      |                   |
|  `POSTGRES_USER`           | Postgres Database Username    | `roundcube`                                                 |                   |
|  `ROUNDCUBE_SITE_NAME`     | Roundcube Site Name           | `RoundKube Webmail`                                         |                   |
|  `ROUNDCUBE_SKIN`          | Roundcube Skin                | `elastic`                                                   |                   |
|  `SMTP_HELO`               | Roundcube SMTP HELO FQDN      | `$APACHE_DOMAIN`                                            |                   |
|  `SMTP_HOST`               | Roundcube SMTP Relay          | `smtp.$MAIL_DOMAIN`                                         |                   |
|  `SMTP_PORT`               | Roundcube SMTP Port           | `25`                                                        |                   |
|  `SMTP_PROTO`              | Roundcube SMTP Protocol       | `smtp`                                                      |                   |
|  `ZPUSH_HOST`              | Roundcube Z-Push Host         | undef                                                       |                   |
|  `ZPUSH_PORT`              | Roundcube Z-Push Port         | `8080`                                                      |                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                     | Inherited From    |
| :------------------ | ------------------------------- | ----------------- |
|  `/certs`           | Apache Certificate (optional)   | opsperator/apache |
