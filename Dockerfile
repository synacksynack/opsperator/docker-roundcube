FROM opsperator/php

# RoundCube image for OpenShift Origin

ARG DO_UPGRADE=
ENV RC_URL=https://github.com/roundcube/roundcubemail/releases/download \
    RC_VERSION=1.6.0

LABEL io.k8s.description="RoundCube $RC_VERSION Webmail." \
      io.k8s.display-name="RoundCube $RC_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="roundcube" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-roundcube" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$RC_VERSION"

USER root

COPY config/* /

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install RoundCube Dependencies" \
    && apt-get -y install libpng16-16 ldap-utils openssl libjpeg62-turbo \
	libwebp6 libgd3 postgresql-client mariadb-client \
    && savedAptMark="$(apt-mark showmanual)" \
    && apt-get -y install libicu-dev libpng-dev libldap2-dev gnupg2 \
	apt-transport-https libjpeg62-turbo-dev libfreetype6-dev libxml2-dev \
	libonig-dev libwebp-dev libcurl4-openssl-dev libzip-dev libpq-dev \
	libmagickwand-dev libmemcached-dev libc-client-dev libkrb5-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
    && docker-php-ext-configure ldap --with-libdir="lib/$debMultiarch" \
    && docker-php-ext-install bcmath exif gd intl ldap opcache pcntl pdo_mysql \
	pdo_pgsql zip imap \
    && docker-php-ext-install mbstring intl gd ldap \
    && pecl install APCu-5.1.21 \
    && pecl install memcached-3.2.0 \
    && pecl install redis-5.3.7 \
    && pecl install imagick-3.7.0 \
    && docker-php-ext-enable apcu gd memcached redis imagick imap \
    && apt-mark auto '.*' >/dev/null \
    && apt-mark manual $savedAptMark \
    && ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
	| awk '/=>/ { print $3 }' | sort -u | xargs -r dpkg-query -S \
	| cut -d: -f1 | sort -u | xargs -rt apt-mark manual \
    && echo "# Install RoundCube" \
    && curl -fsSL -o /usr/src/rcube.tar.gz \
	"$RC_URL/$RC_VERSION/roundcubemail-$RC_VERSION-complete.tar.gz" \
    && tar -C /usr/src --no-same-owner -xf /usr/src/rcube.tar.gz \
    && rm -fr /var/www/html \
    && mv /usr/src/roundcubemail-$RC_VERSION /var/www/html \
    && chown -R root:root /var/www/html \
    && echo "# Enabling LDAP Modules" \
    && a2enmod proxy proxy_http proxy_wstunnel proxy_balancer \
    && a2dismod perl \
    && echo "# Fixing permissions" \
    && for dir in /var/www/html/config /var/www/html/logs /var/www/html/temp; \
	do \
	    mkdir -p $dir 2>/dev/null \
	    && chown -R 1001:root $dir \
	    && chmod -R g=u $dir; \
	done \
    && echo "# Cleaning up" \
    && apt-get remove --purge -y gnupg2 \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/usr/src/php.tar.xz /var/www/html/.htaccess /var/www/html/installer \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-roundcube.sh"]
USER 1001
