<?php
$config = array();
$config['db_dsnw'] = 'DBURL';
$config['des_key'] = 'DES_KEY';
$config['default_host'] = 'IMAP_PROTOIMAP_HOST';
$config['default_port'] = IMAP_PORT;
$config['force_https'] = false;
$config['imap_vendor'] = IMAP_VENDOR;
$config['log_driver'] = 'stdout';
$config['login_autocomplete'] = 0;
$config['mail_domain'] = 'MAIL_DOMAIN';
$config['product_name'] = 'ROUNDCUBE_SITE_NAME';
$config['session_storage'] = 'db';
$config['skin'] = 'ROUNDCUBE_SKIN';
$config['smtp_helo_host'] = 'SMTP_HELO';
$config['smtp_pass'] = '%p';
$config['smtp_port'] = SMTP_PORT;
$config['smtp_server'] = 'SMTP_PROTOSMTP_HOST';
$config['smtp_user'] = '%u';
$config['support_url'] = 'ROUNDCUBE_SUPPORT_URL';
$config['use_https'] = DO_SSL;
$config['useragent'] = 'ROUNDCUBE_SITE_NAME/' . RCUBE_VERSION;
//?? $config['username_domain'] = '';

$config['imap_log_session'] = true;
$config['smtp_log'] = true;
$config['log_logins'] = true;
$config['apc_debug'] = DO_DEBUG;
$config['imap_debug'] = DO_DEBUG;
$config['ldap_debug'] = DO_DEBUG;
$config['memcache_debug'] = DO_DEBUG;
$config['redis_debug'] = DO_DEBUG;
$config['session_debug'] = DO_DEBUG;
$config['smtp_debug'] = DO_DEBUG;
$config['sql_debug'] = DO_DEBUG;

$config['enable_installer'] = false;
