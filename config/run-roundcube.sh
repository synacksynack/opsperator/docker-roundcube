#!/bin/sh

if test "$DEBUG"; then
    set -x
    DO_DEBUG=true
else
    DO_DEBUG=false
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
DES_KEY=${DES_KEY:-stringshouldbe24charslog}
MAIL_DOMAIN=${MAIL_DOMAIN:-demo.local}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
ROUNDCUBE_SKIN=${ROUNDCUBE_SKIN:-elastic}
IMAP_PROTO=${IMAP_PROTO:-imap}
IMAP_VENDOR=${IMAP_VENDOR:-none}
SMTP_PROTO=${SMTP_PROTO:-smtp}

if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=mail.$MAIL_DOMAIN
fi
if test -z "$IMAP_HOST"; then
    IMAP_HOST=imap.$MAIL_DOMAIN
fi
if test -z "$IMAP_PORT" -a "$IMAP_PROTO" = imaps; then
    IMAP_PORT=993
elif test -z "$IMAP_PORT"; then
    IMAP_PORT=143
fi
if test "$IMAP_PROTO" = imaps; then
    IMAP_PROTO=ssl://
else
    IMAP_PROTO=
fi
if ! test "$IMAP_VENDOR" = none; then
    IMAP_VENDOR="'$IMAP_VENDOR'"
fi
if ! test -d "/var/www/html/skins/$ROUNDCUBE_SKIN"; then
    ROUNDCUBE_SKIN=elastic
fi
if test -z "$ROUNDCUBE_SITE_NAME"; then
    ROUNDCUBE_SITE_NAME="RoundKube Webmail"
fi
if test -z "$ROUNDCUBE_SUPPORT_URL"; then
    ROUNDCUBE_SUPPORT_URL=https://github.com/roundcube/roundcubemail
fi
if test -z "$SMTP_HOST"; then
    SMTP_HOST=smtp.$MAIL_DOMAIN
fi
if test -z "$SMTP_HELO"; then
    SMTP_HELO=$APACHE_DOMAIN
fi
if test -z "$SMTP_PORT" -a "$SMTP_PROTO" = smtps; then
    SMTP_PORT=465
elif test -z "$SMTP_PORT"; then
    SMTP_PORT=25
fi
if test "$SMTP_PROTO" = smtps; then
    SMTP_PROTO=ssl://
else
    SMTP_PROTO=
fi

cpt=0
if test "$POSTGRES_PASSWORD"; then
    POSTGRES_DB=${POSTGRES_DB:-roundcube}
    POSTGRES_HOST=${POSTGRES_HOST:-127.0.0.1}
    POSTGRES_PORT=${POSTGRES_PORT:-5432}
    POSTGRES_USER=${POSTGRES_USER:-roundcube}
    echo Waiting for Postgres backend ...
    while true
    do
	if echo '\d' | PGPASSWORD="$POSTGRES_PASSWORD" \
		psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		-p "$POSTGRES_PORT" "$POSTGRES_DB" >/dev/null 2>&1; then
	    echo " Postgres is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach Postgres" >&2
	    exit 1
	fi
	sleep 5
	echo Postgres is ... KO
	cpt=`expr $cpt + 1`
    done
    if ! echo SELECT expires FROM cache | PGPASSWORD="$POSTGRES_PASSWORD" \
	    psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
	    -p "$POSTGRES_PORT" "$POSTGRES_DB" >/dev/null 2>&1; then
	if ! cat /var/www/html/SQL/postgres.initial.sql \
		| PGPASSWORD="$POSTGRES_PASSWORD" \
		psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		-p "$POSTGRES_PORT" "$POSTGRES_DB"; then
	    echo " Failed initializing Database"
	    exit 1
	fi
    fi
elif test "$MYSQL_PASSWORD"; then
    MYSQL_DATABASE=${MYSQL_DATABASE:-roundcube}
    MYSQL_HOST=${MYSQL_HOST:-127.0.0.1}
    MYSQL_PORT=${MYSQL_PORT:-3306}
    MYSQL_USER=${MYSQL_USER:-roundcube}
    echo Waiting for MySQL backend ...
    while true
    do
	if echo SHOW TABLES | mysql -u "$MYSQL_USER" \
		--password="$MYSQL_PASSWORD" -h "$MYSQL_HOST" \
		-P "$MYSQL_PORT" "$MYSQL_DATABASE" >/dev/null 2>&1; then
	    echo " MySQL is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach MySQL" >&2
	    exit 1
	fi
	sleep 5
	echo MySQL is ... KO
	cpt=`expr $cpt + 1`
    done
    if ! echo SELECT expires FROM cache | mysql -u "$MYSQL_USER" \
	    --password="$MYSQL_PASSWORD" -h "$MYSQL_HOST" \
	    -P "$MYSQL_PORT" "$MYSQL_DATABASE" >/dev/null 2>&1; then
	if ! cat /var/www/html/SQL/mysql.initial.sql \
		| mysql -u "$MYSQL_USER" --password="$MYSQL_PASSWORD" \
		-h "$MYSQL_HOST" -P "$MYSQL_PORT" "$MYSQL_DATABASE"; then
	    echo " Failed initializing Database"
	    exit 1
	fi
    fi
fi

if test "$POSTGRES_PASSWORD"; then
    DBURL="postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST:$POSTGRES_PORT/$POSTGRES_DB"
elif test "$MYSQL_PASSWORD"; then
    DBURL="mysql://$MYSQL_USER:$MYSQL_PASSWORD@$MYSQL_HOST:$MYSQL_PORT/$MYSQL_DATABASE"
elif test -z "$DBURL"; then
    echo CRITICAL: missing DBURL env var
    sleep 120
    exit 1
fi
if test "$SSL_INCLUDE" = no-ssl; then
    DO_SSL=false
else
    DO_SSL=true
fi

export APACHE_DOMAIN
export APACHE_HTTP_PORT
export APACHE_IGNORE_OPENLDAP=yay
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
export RESET_TLS=false

if ! test -s /var/www/html/config/config.inc.php; then
    echo "Install Roundcube Site Configuration"
    sed -e "s|MAIL_DOMAIN|$MAIL_DOMAIN|" \
	-e "s|DBURL|$DBURL|" \
	-e "s|DO_DEBUG|$DO_DEBUG|" \
	-e "s|DO_SSL|$DO_SSL|" \
	-e "s|IMAP_HOST|$IMAP_HOST|" \
	-e "s|IMAP_PROTO|$IMAP_PROTO|" \
	-e "s|IMAP_PORT|$IMAP_PORT|" \
	-e "s|IMAP_VENDOR|$IMAP_VENDOR|" \
	-e "s|ROUNDCUBE_SITE_NAME|$ROUNDCUBE_SITE_NAME|" \
	-e "s|ROUNDCUBE_SKIN|$ROUNDCUBE_SKIN|" \
	-e "s|ROUNDCUBE_SUPPORT_URL|$ROUNDCUBE_SUPPORT_URL|" \
	-e "s|SMTP_HELO|$SMTP_HELO|" \
	-e "s|SMTP_HOST|$SMTP_HOST|" \
	-e "s|SMTP_PROTO|$SMTP_PROTO|" \
	-e "s|SMTP_PORT|$SMTP_PORT|" \
	/config.php >/var/www/html/config/config.inc.php
fi

if ! ls /etc/apache2/sites-enabled/*.conf >/dev/null 2>&1; then
    echo "Generates Roundcube VirtualHost Configuration"
    (
	cat <<EOF
<VirtualHost *:$APACHE_HTTP_PORT>
    ServerName $APACHE_DOMAIN
    CustomLog /dev/stdout modremoteip
    Include "/etc/apache2/$SSL_INCLUDE.conf"
    AddDefaultCharset UTF-8
    Alias /favicon.ico /var/www/html/skins/$ROUNDCUBE_SKIN/images/favicon.ico
    DirectoryIndex index.php
    DocumentRoot /var/www/html
    <Location />
	AllowOverride All
	Options +SymLinksIfOwnerMatch
	Require all granted
    </Location>
EOF
	if test "$ZPUSH_HOST"; then
	    ZPUSH_PORT=${ZPUSH_PORT:-8080}
	    cat <<EOF
    ProxyPass /Microsoft-Server-ActiveSync http://$ZPUSH_HOST:$ZPUSH_PORT/Microsoft-Server-ActiveSync retry=60 connectiontimeout=5 timeout=360
    <Proxy http://$ZPUSH_HOST:$ZPUSH_PORT>
	Order allow,deny
	Allow from all
    </Proxy>
EOF
	fi
	cat <<EOF
    RewriteEngine On
    RewriteRule ^/*\$ /index.php
    RewriteRule ^(?!installer|\.well-known\/|[a-zA-Z0-9]{16})(\.?[^\.]+)\$ - [F]
    RewriteRule ^/?(\.git|\.tx|SQL|bin|config|logs|temp|tests|vendor|program\/(include|lib|localization|steps)) - [F]
    RewriteRule /?(README.*|meta\.json|composer\..*|jsdeps.json)\$ - [F]
    <IfModule mod_deflate.c>
	SetOutputFilter DEFLATE
    </IfModule>
    <IfModule mod_expires.c>
	ExpiresActive On
	ExpiresDefault "access plus 1 month"
    </IfModule>
    FileETag MTime Size
</VirtualHost>
EOF
    ) >/etc/apache2/sites-enabled/003-vhosts.conf
fi

. /run-apache.sh
